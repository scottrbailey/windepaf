-- All queries must return the following columns:
-- pidm, posn, suffix, eff_date
SELECT jad.pidm, jad.posn, jad.suffix,
  jad.begin_date eff_date
FROM ods_job_assign_details jad
WHERE jad.is_current = 'Y'
  AND jad.rate_of_pay < 14.00
  AND jad.earn_code != 'VOL'
  AND rownum < 255
