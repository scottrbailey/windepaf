#!/usr/bin/env python

#-------------------------------------------------------------------------------
# Name:        epaf
# Purpose:     Utilities for windEPAF bulk import/export
#
# Author:      Scott Bailey, The Evergreen State College, Olympia WA
# email:       baileys@evergreen.edu
#
# Created:     02/21/2013
# Licence:     BSD
#-------------------------------------------------------------------------------

import dbms
import datetime
import os
import subprocess
import xlwt

"""
Configurable paths to where files should be read from and placed
If run on your jobsub server it will find the $INTER_DIR environment variable and use that
and if you run locally, it will use the local folder
"""
IMPORT_DIR = os.getenv('INTER_DIR', '../files') + '/windepaf/import'
EXPORT_DIR = os.getenv('INTER_DIR', '../files') + '/windepaf/export'
QUERY_DIR = os.getenv('INTER_DIR', '../files') + '/windepaf/queries'

"""
There are three options for establishing a database connection:
1) Create a saved connection in dbms
   > dbms.servers.save('banner', 'oracle', user='baninst1', password='CHANGE_ME', database='bandev', host='bandev.myschool.edu')
2) Call a script that returns the specified users password
3) Hard code the password in this file

If not connecting as BANINST1, be sure the user is granted execute on zp_windEPAF.
"""

if 'bandev' in dbms.servers.list():
    dbms.servers.setMasterPassword('CHANGE_ME')
    db = dbms.servers.open('bandev')
else:
    # Banner instance and password
    db_inst = os.getenv('DB_INSTANCE', 'bandev')
    try:
        # call exe that will return the password if run as the jobsub user
        prog = subprocess.Popen('banner_password.shl $DB_INSTANCE baninst1', shell=True, stdout=subprocess.PIPE)
        db_pwd = prog.communicate()[0].rstrip()
    except:
        # you can optionally hard code the password here
        db_pwd = 'CHANGE_ME_TOO'

    db = dbms.connect.oracle(user='BANINST1', password=db_pwd, database=db_inst, host=None)
    print('Connected to %s' % db_inst)

# Generally any block prefix will work. But when pre-populating a spreadsheet from a query, we
# need to know which prefix corresponds to employee/job start/end
NORTRAN_EMP_PREFIX = 'ER'
NORTRAN_EMP_TERM_PREFIX = 'ES'
NORTRAN_JOBSTART_PREFIX = 'JR'
NORTRAN_JOBEND_PREFIX = 'JS'

# values to default in when none is found
NORTRAN_DEFAULTS = {
    'erbc - pebempl_coas_code_home': 'E',
    'erbc - pebempl_orgn_code_dist': '32'
}
#------------------ END OF CONFIGURATION DO NOT EDIT BELOW ---------------------#

cur = db.cursor()
today = datetime.datetime.today()

VERSION = '1.18.7'

TRANSACTION_STATUS = {
    'A': 'Approved',
    'C': 'Complete',
    'D': 'Disapproved',
    'N': 'Cancelled',
    'P': 'Partially Complete',
    'R': 'Returned for Correction',
    'V': 'Void',
    'W': 'Waiting'}

QUEUE_STATUS = {
    'A': 'Approved',
    'D': 'Disapproved',
    'I': 'In the Queue',
    'K': 'Acknowledge', # For FYI (Action = F)
    'L': 'Applied',     # (Action = L)
    'O': 'Overridden',
    'P': 'Pending',
    'Q': 'Removed from Queue',
    'R': 'Return'}

# all EPAF fields
_epFields = []
# just NORTRAN fields
_ntFields = []

# map of Excel Styles
xl_styles = {
    'head':     xlwt.easyxf('font: bold on; alignment: horz center; protection: cell_locked on;'),
    'title':    xlwt.easyxf('font: bold on; alignment: horz left; protection: cell_locked on;'),
    'lock':     xlwt.easyxf('font: color_index 25; protection: cell_locked on; alignment: wrap on'),
    'lock_date': xlwt.easyxf('font: color_index 25; protection: cell_locked on;',
                            num_format_str = 'd-mmm-yyyy'),
    'str':      xlwt.easyxf('alignment: horz left, wrap on; protection: cell_locked off;',
                            num_format_str = '@'),
    'date':     xlwt.easyxf('alignment: horz left; protection: cell_locked off;',
                            num_format_str = 'd-mmm-yyyy'),
    'int':      xlwt.easyxf('alignment: horz left; protection: cell_locked off;',
                            num_format_str = '0'),
    'float':    xlwt.easyxf('alignment: horz left; protection: cell_locked off;',
                            num_format_str = '0.00')}

def valToString(val):
    """ Convert value to appropriate string """
    if val is None:
        ret = ''
    elif hasattr(val, 'strftime'):
        try:
            ret = val.strftime('%d-%b-%Y').upper()
        except:
            ret = str(val)
    elif hasattr(val, 'is_integer') and val.is_integer():
        ret = str(int(val))
    else:
        ret = str(val)
    return ret

def fieldsFromCursor(cur, section, counter=None):
    """ Take a list of columns from a cursor and make a field entry for each """
    vals = cur.fetchone()
    if vals is None:
        # dont add section if not in EPAF
        return []

    if counter in [None, 1]:
        ret = [{'name' : '# ' + section + ' #', 'type': 'head'}]
    else:
        ret = []

    no_edits = ['ref_no', 'trans_no', 'acat_code']
    for col in cur.description:
        name = "%s_%d" % (col[0].lower(), counter) if counter else col[0].lower()
        edit = 'N' if name in no_edits else 'Y'
        if col[1] == db.interface.DATETIME:
            type = 'date'
        elif col[1] == db.interface.STRING:
            type = 'str'
        elif col[1] == db.interface.NUMBER and col[5] == 0:
            type = 'int'
        elif col[1] == db.interface.NUMBER:
            type = 'float'
        else:
            type = 'str'

        ret.append({'name': name,
                    'section': section,
                    'type': type,
                    'edit': edit,
                    'ref_val': vals[col[0].lower()]})
    return ret

def _nobtranFields(refNo, valuesOnly=False):
    sql = '''
    SELECT t.nobtran_transaction_no      ref_no,
      -1                                 trans_no,
      sp.spriden_last_name ||', '|| sp.spriden_first_name  employee,
      sp.spriden_id                      id,
      rt.nortran_posn                    posn,
      rt.nortran_suff                    suff,
      t.nobtran_effective_date           effective_date,
      t.nobtran_acat_code                acat_code,
      t.nobtran_originator_user_id       user_id,
      c.norcmnt_comments                 comments
    FROM nobtran t
    JOIN spriden sp ON t.nobtran_pidm = sp.spriden_pidm
      AND sp.spriden_change_ind IS NULL
      AND sp.spriden_entity_ind = 'P'
    LEFT JOIN nortran rt ON t.nobtran_transaction_no = rt.nortran_transaction_no
      AND rt.nortran_apty_code LIKE :1
      AND rt.nortran_aufd_code = 'NBRJOBS_EFFECTIVE_DATE'
    LEFT JOIN norcmnt c ON t.nobtran_transaction_no = c.norcmnt_transaction_no
      AND c.norcmnt_seq_no = 1
    WHERE t.nobtran_transaction_no = :2 '''
    cur.execute(sql, ("%s%%" % NORTRAN_JOBSTART_PREFIX, refNo,))

    if valuesOnly:
        row = cur.fetchone()
        if row:
            return row.copy()
    else:
        fields = fieldsFromCursor(cur, 'NOBTRAN')
        if not fields:
            raise ValueError('The reference EPAF #%d was not found' % refNo)
        return fields

def _norroutFields(refNo, valuesOnly=False):
    sql = '''
    SELECT 'LEVEL_' || rt.norrout_alvl_code name,
      'str'         type,
      'NORROUT'     section,
      'Y'           edit,
      rt.norrout_recipient_user_id ref_val
    FROM norrout rt
    WHERE rt.norrout_transaction_no = :0
    ORDER BY rt.norrout_level_no'''
    cur.execute(sql, (refNo,))
    rows = [row.copy() for row in cur.fetchall()]
    if len(rows):
        if valuesOnly:
            return dict((row['name'], row['ref_val']) for row in rows)
        else:
            # return full field information
            return [{'name' : '# NORROUT #', 'type': 'head', 'section': None}] + rows
    else:
        return []

def _norternFields(refNo, valuesOnly=False):
    # NORTERN Section
    sql = '''
    SELECT e.nortern_earn_code earn_code,
      e.nortern_hrs            hrs,
      e.nortern_special_rate   special_rate,
      e.nortern_cancel_date    cancel_date
    FROM nortern e
    WHERE e.nortern_transaction_no = :0
    ORDER BY e.rowid'''
    cur.execute(sql, (refNo,))
    i = 1
    if valuesOnly:
        tmp = {}
        for row in cur.fetchall():
            tmp.update(dict([("%s_%d" % (k,i), v) for k,v in row.items()]))
            i += 1
        return tmp
    else:
        tmp = []
        done = False
        while not done:
            row = fieldsFromCursor(cur, 'NORTERN', i)
            if row:
                tmp += row
                i += 1
            else:
                done = True
        return tmp

def _nortlbdFields(refNo, valuesOnly=False):
    # NORTLBD Section
    sql = '''
    SELECT l.nortlbd_coas_code  coas_code,
      l.nortlbd_fund_code       fund_code,
      l.nortlbd_orgn_code       orgn_code,
      l.nortlbd_acct_code       acct_code,
      l.nortlbd_prog_code       prog_code,
      l.nortlbd_actv_code       actv_code,
      l.nortlbd_locn_code       locn_code,
      l.nortlbd_proj_code       proj_code,
      l.nortlbd_percent         pct     
    FROM nortlbd l
    WHERE l.nortlbd_transaction_no = :0
    ORDER BY l.rowid'''

    cur.execute(sql, (refNo,))
    i = 1
    if valuesOnly:
        tmp = {}
        for row in cur:
            tmp.update(dict([("%s_%d" % (k,i), v) for k,v in row.items()]))
            i += 1
        return tmp
    else:
        tmp = []
        done = False
        while not done:
            row = fieldsFromCursor(cur, 'NORTLBD', i)
            if row:
                tmp += row
                i += 1
            else:
                done = True
        return tmp

def nortranFields(refNo, valuesOnly=False):
    """ Get NORTRAN field definitions """
    global _ntFields

    if _ntFields and not valuesOnly:
        return _ntFields
    sql = '''
        SELECT lower(fv.ntradfv_apty_code ||' - '|| fv.ntradfv_aufd_code)
                                     name,
          fv.ntradfv_apty_code       apty_code,
          fv.ntradfv_aubk_code       tbl,
          fv.ntradfv_aufd_code       field,
          fv.ntradfv_aufd_default    def_val,
          r.nortran_value            ref_val,
          fv.ntradfv_default_override_ind  edit,
          'str'                      type,
          'NORTRAN'                  section
        FROM nobtran b
        JOIN ntradfv fv ON b.nobtran_acat_code = fv.ntradfv_acat_code
        LEFT JOIN nortran r ON b.nobtran_transaction_no = r.nortran_transaction_no
          AND fv.ntradfv_apty_code = r.nortran_apty_code
          AND fv.ntradfv_aubk_code = r.nortran_aubk_code
          AND fv.ntradfv_aufd_code = r.nortran_aufd_code
        WHERE b.nobtran_transaction_no = :0
        ORDER BY fv.ntradfv_apty_code, r.nortran_display_seq_no'''

    cur.execute(sql, (refNo,))
    if valuesOnly:
        return dict((row['name'], row['ref_val']) for row in cur)
    else:
        tmp = []
        for row in cur.fetchall():
            if 'DATE' in row['field']:
                row['type'] = 'date'
            tmp.append(row.copy())
        _ntFields = tmp
        return tmp

def epafFields(refNo):
    """ Creates a list of fields needed for our EPAF based on the reference EPAF """
    global _epFields, _ntFields

    if _epFields:
        return _epFields

    fields = []
    fields += _nobtranFields(refNo)
    fields += _norroutFields(refNo)
    fields += _norternFields(refNo)
    fields += _nortlbdFields(refNo)
    nt = nortranFields(refNo)
    if len(nt):
        fields += [{'name' : '# NORTRAN #', 'type': 'head', 'section': None}] + nt

    _epFields = fields
    return fields

def currentVals(pidm, posn, suff='00', effDate=None):
    """ Lookup NORTRAN values from existing employee/job data """
    global _ntFields
    cur.columnCase = cur.UPPER_CASE # the field values in nortran are uppercase
    cv = {}
    # PEBEMPL values
    sql = '''SELECT * FROM pebempl e WHERE e.pebempl_pidm = :0'''
    cur.execute(sql, (pidm,))
    row = cur.fetchone()
    if row:
        for fld in _ntFields:
            if fld['field'] in row.keys():
                cv[fld['name']] = row[fld['field']]

    # Look up emp/job current effective date if not provided
    if effDate is None:
        sql = '''
        SELECT MAX(j.nbrjobs_effective_date)
        FROM nbrjobs j
        WHERE j.nbrjobs_pidm = :0
          AND j.nbrjobs_posn = :1
          AND j.nbrjobs_suff = :2
          AND j.nbrjobs_effective_date < sysdate '''
        cur.execute(sql, (pidm, posn, suff))
        row = cur.fetchone()
        if row:
            effDate = row[0]
        else:
            # employee does not currently have this job
            cur.columnCase = cur.LOWER_CASE
            return cv

    # current NBRBJOB/NBRJOBS values
    sql = '''
        SELECT b.*, r.*
        FROM nbrbjob b
        JOIN nbrjobs r ON b.nbrbjob_pidm = r.nbrjobs_pidm
          AND b.nbrbjob_posn = r.nbrjobs_posn
          AND b.nbrbjob_suff = r.nbrjobs_suff
        WHERE b.nbrbjob_pidm = :0
          AND b.nbrbjob_posn = :1
          AND b.nbrbjob_suff = :2
          AND r.nbrjobs_effective_date = :3 '''
    cur.execute(sql, (pidm, posn, suff, effDate))
    row = cur.fetchone()
    if row:
        for fld in _ntFields:
            if (fld['apty_code'].startswith(NORTRAN_JOBSTART_PREFIX) and fld['field'] in row.keys()):
                cv[fld['name']] = row[fld['field']]

    # job end record
    sql = '''
        SELECT *
        FROM nbrjobs ej
        WHERE ej.nbrjobs_status = 'T'
          AND ej.nbrjobs_pidm = :0
          AND ej.nbrjobs_posn = :1
          AND ej.nbrjobs_suff = :2
          AND ej.nbrjobs_effective_date > :3 '''
    cur.execute(sql, (pidm, posn, suff, effDate))
    row = cur.fetchone()

    if row:
        for fld in _ntFields:
            if fld['apty_code'].startswith(NORTRAN_JOBEND_PREFIX) and fld['field'] in row.keys():
                cv[fld['name']] = row[fld['field']]
    cur.columnCase = cur.LOWER_CASE
    return cv

def contractType(pidm, posn, suff, changeEffDate):
    sql = '''
    SELECT MAX(CASE WHEN b.nbrbjob_posn = :posn
      AND b.nbrbjob_suff = :suff THEN b.nbrbjob_contract_type END) curr_type,
      MAX(CASE WHEN b.nbrbjob_contract_type = 'P' THEN 1 END) primary_exists
    FROM nbrbjob b
    WHERE b.nbrbjob_pidm = :pidm
      AND :effDate BETWEEN b.nbrbjob_begin_date AND COALESCE(b.nbrbjob_end_date, :effDate)'''
    cur.execute(sql, {'pidm': pidm, 'posn': posn, 'suff': suff, 'effDate': changeEffDate})
    row = cur.fetchone()
    if row:
        if row['curr_type']:
            # keep current type
            ret = row['curr_type']
        elif row['primary_exists'] == 1:
            # primary job exists
            ret = 'S'
        else:
            # no primary job
            ret = 'P'
    else:
        # No jobs on eff date
        ret = 'P'
    return ret

if __name__ == '__main__':
    import pprint

    ref_no = 1
    nobtran = _nobtranFields(ref_no, True)
    fields = epafFields(ref_no)
    pprint.pprint(fields)
