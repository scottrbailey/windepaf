#!/usr/bin/env python

#-------------------------------------------------------------------------------
# Name:        epaf_export
# Purpose:     Using an existing EPAF as a reference, create an Excel Spreadsheet
#              with the specified number of copies to be used as a template for
#              bulk inserting EPAFs.
#
# Author:      Scott Bailey, The Evergreen State College, Olympia WA
#
# Created:     02/21/2013
# Licence:     BSD
#-------------------------------------------------------------------------------

import getopt
import os
import sys
import xlwt
import epaf

def promptForParms():
    import getpass
    if sys.version_info[0] > 2:
        prompt = input
    else:
        prompt = raw_input
    ref_no = prompt('Enter reference transaction no: \n')
    cnt = int(prompt('How many records would you like to create?\n(Empty records will just be ignored.)\n'))
    if cnt > 254:
        print('This version of Excel has a limit of 254 columns.')
        cnt = 254
    user = getpass.getuser()
    if user:
        user = user.upper()
    return {'ref_no': ref_no, 'cnt': cnt, 'user_id': user}

def main(ref_no, cnt, user_id = ''):
    epFields = epaf.epafFields(ref_no)
    styles = epaf.xl_styles
    defVals = {'user_id': user_id,
        'suff': '00',
        'er - pebempl_current_hire_date': '*CV'}
    wb = xlwt.Workbook()
    ws = wb.add_sheet('EPAF Export')

    ws.write(0,0, 'Fields', styles['head'])
    ws.write(0,1, 'Ref EPAF', styles['head'])
    ws.col(0).width = 256 * 35
    ws.col(1).width = 256 * 25

    if cnt > 254:
        print('Excel is limited to 256 columns')
        cnt = 254

    # create import column headers and set width
    for c in range(2, cnt + 2):
        ws.write(0, c, 'Import %d' % (c - 1), styles['head'])
        ws.col(c).width = 256 * 25
    # row num
    r = 1
    for fld in epFields:
        if fld['type'] == 'head':
            #section header
            ws.write(r, 0, fld['name'], styles['head'])
            for c in range(2, cnt + 2):
                ws.write(r, c, '', styles['str'])
        else:
            ws.write(r, 0, fld['name'], styles['title'])
            ws.write(r, 1, epaf.valToString(fld['ref_val']), styles['lock'])
            if fld['edit'] == 'N':
                # Can not edit these fields in spreadsheet
                newVal = epaf.valToString(fld['ref_val'])
                newStyle = styles['lock']
            elif fld['name'] in defVals:
                # default a value in
                newVal = epaf.valToString(defVals[fld['name']])
                newStyle = styles[fld['type']]
            else:
                newVal = ''
                newStyle = styles[fld['type']]
            # fill in values for import columns
            for c in range(2, cnt + 2):
                ws.write(r, c, newVal, newStyle)
        r += 1

    ws.panes_frozen = True
    ws.vert_split_pos = 2
    ws.horz_split_pos = 1
    ws.protect = True
    xl_filename = '%s/epaf_export_%d.xls' % (epaf.EXPORT_DIR.rstrip('/\\'), ref_no)
    wb.save(xl_filename)
    print('Exported %d copies of %s to %s' % (cnt, ref_no, xl_filename))

if __name__ == '__main__':
    exe_name = os.path.basename(sys.argv[0])
    usage = 'Usage:\n%s <one_up_number> <user_id>' % exe_name
    usage += '\nor\n%s REF_TRANSACTION COUNT\n-t, --transaction\n-c, --count\n--user_id' % exe_name
    cur = epaf.cur
    parms = {}

    try:
        opts, args = getopt.getopt(sys.argv[1:], "ht:c:u",['help','transaction=','count=','user_id='])
    except:
        print(usage)
        sys.exit(2)

    if len(args) == 0 and len(opts) == 0:
        # ran interactively
        parms = promptForParms()
    elif len(args) >= 1:
        parms['one_up'] = int(args[0])
    else:
        # process options instead
        for k, v in opts:
            if k in ('-h','--help'):
                print(usage)
                sys.exit()
            elif k in ('-t','--transaction'):
                parms['ref_no'] = int(v)
            elif k in ('-c','--count'):
                parms['cnt'] = int(v)
            elif k in ('-u','--user_id'):
                parms['user_id'] = str(v).upper()


    if parms.get('one_up', 0) > 0:
        # Need to look up transaction and count from Banner
        sql = '''
        SELECT p1.gjbprun_value          ref_no,
          p2.gjbprun_value               cnt,
          p3.gjbprun_value               user_id
        FROM gjbjobs j
        JOIN gjbprun p1 ON j.gjbjobs_name = p1.gjbprun_job
          AND p1.gjbprun_number = '01'
        JOIN gjbprun p2 ON j.gjbjobs_name = p2.gjbprun_job
          AND p1.gjbprun_one_up_no = p2.gjbprun_one_up_no
          AND p2.gjbprun_number = '02'
        LEFT JOIN gjbprun p3 ON j.gjbjobs_name = p3.gjbprun_job
          AND p1.gjbprun_one_up_no = p3.gjbprun_one_up_no
          AND p3.gjbprun_number = '03'
        WHERE j.gjbjobs_name = 'EPAF_EXPORT'
          AND p1.gjbprun_one_up_no =  :1 '''

        row = cur.selectinto(sql, (parms['one_up'],))
        parms['ref_no'] = int(row['trans'])
        parms['cnt'] = int(row['cnt'])
        if row['user_id']:
            parms['user_id'] = row['user_id']

    if 'ref_no' not in parms:
        print('Error: could not determine the transaction to load')
        sys.exit()

    main(int(parms['ref_no']), parms.get('cnt', 25), parms.get('user_id'))
