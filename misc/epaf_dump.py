#!/usr/bin/env python

#-------------------------------------------------------------------------------
# Name:        epaf_dump
# Purpose:     Create a prepopulated Excel Spreadsheet to edit and upload EPAFs.
#              EPAF Dump is similar to EPAF Export, but instead of empty
#              records, values are defaulted either from a previous EPAF import
#              (import mode), or from the employee/job records (query mode).
#
# Author:      Scott Bailey, The Evergreen State College, Olympia WA
# email:       baileys@evergreen.edu
#
# Created:     02/26/2013
# Licence:     BSD
#-------------------------------------------------------------------------------

import epaf
import getopt
import glob
import os
import sys
import xlwt

def promptForParms():
    import getpass
    if sys.version_info[0] > 2:
        prompt = input
    else:
        prompt = raw_input
    user = getpass.getuser()
    if user:
        user = user.upper()
    ref_no = prompt('Enter reference transaction number: \n')
    i = 0
    while i < 5:
        fn = prompt("Enter query file name ('ls' to list files) \n" +
                "or hit ENTER to export all EPAFs imported at the same time as the reference EPAF\n")
        if fn == 'ls':
            files = [os.path.basename(f) for f in glob.glob('%s/*.sql' % epaf.QUERY_DIR)]
            print('Files in query directory:')
            print('\n'.join(files))
            print('\n')
        elif fn == '':
            fn = None
            i = 5
        elif os.path.exists("%s/%s" % (epaf.QUERY_DIR, fn)):
            i = 5
        else:
            print('%s was not found in %s' % (fn, epaf.QUERY_DIR))
        i += 1

    return {'ref_no': int(ref_no), 'file': fn, 'user_id': user}


def empValsFromImport(ref_no):
    """ Get all EPAFs that were imported along with reference EPAF """

    epFields = epaf.epafFields(ref_no)
    ret = []
    sql = '''SELECT ra.nobtran_transaction_no trans_no
        FROM nobtran r
        JOIN nobtran ra ON r.nobtran_acat_code = ra.nobtran_acat_code
          AND r.nobtran_submittor_user_id = ra.nobtran_submittor_user_id
          AND ra.nobtran_created_date BETWEEN r.nobtran_created_date - 1/1440 AND r.nobtran_created_date + 1/1440
        WHERE r.nobtran_transaction_no = :0
        ORDER BY ra.nobtran_transaction_no '''
    cur.execute(sql, (ref_no,))
    rows = cur.fetchall()
    for row in rows:
        trans = row[0]
        rec = epaf._nobtranFields(trans, True)
        rec['trans_no'] = trans
        rec['ref_no'] = ref_no
        rec.update(epaf._norroutFields(trans, True))
        rec.update(epaf.nortranFields(trans, True))
        tmp = epaf._norternFields(trans, True)
        if tmp:
            rec.update(tmp)
        tmp = epaf._nortlbdFields(trans, True)
        if tmp:
            rec.update(tmp)
        ret.append(rec)
    return ret

def empValsFromQuery(ref_no, query):
    """ Run query (needs to return pidm, posn, suff, eff_date)
        and return epaf values for each person/job """
    epFields = epaf.epafFields(ref_no)

    ret = {}
    sql = '''
        SELECT si.spriden_id        id,
          -1                        trans_no,
          si.spriden_last_name ||', '|| si.spriden_first_name employee,
          bj.nbrbjob_posn           posn,
          bj.nbrbjob_suff           suff,
          emp.*,
          bj.*,
          rj.*
        FROM (
          -- USER DEFINED QUERY MUST RETURN COLUMNS:
          -- pidm, posn, suffix, eff_date
          %s
        ) sub
        JOIN spriden si ON sub.pidm = si.spriden_pidm
          AND si.spriden_change_ind IS NULL
          AND si.spriden_entity_ind = 'P'
        JOIN pebempl emp ON sub.pidm = emp.pebempl_pidm
        JOIN nbrbjob bj ON sub.pidm = bj.nbrbjob_pidm
          AND sub.posn = bj.nbrbjob_posn
          AND sub.suffix = bj.nbrbjob_suff
          AND sub.eff_date BETWEEN bj.nbrbjob_begin_date
            AND COALESCE(bj.nbrbjob_end_date, sub.eff_date)
        JOIN nbrjobs rj ON sub.pidm = rj.nbrjobs_pidm
          AND sub.posn = rj.nbrjobs_posn
          AND sub.suffix = rj.nbrjobs_suff
          AND sub.eff_date = rj.nbrjobs_effective_date
        ORDER BY employee, posn, suff''' % query
    cur.execute(sql)
    i = 1
    for row in cur:
        rec = {'sort_order': i}
        i+=1
        for fld in epFields:
            fld_name = cur._sanitize(fld['name'], 1)
            if fld.get('section') in  ['NOBTRAN','NORTRAN'] and fld_name in row.keys():
                rec[fld['name']] = row[fld_name]
            elif (fld.get('section') == 'NORTRAN'
                and fld['apty_code'][:2] in [epaf.NORTRAN_EMP_PREFIX, epaf.NORTRAN_JOBSTART_PREFIX]
                and fld['field'].lower() in row.keys()):
                rec[fld['name']] = row[fld['field'].lower()]
        ret[row['id']] = rec

    # Get end job data
    sql = '''
    SELECT si.spriden_id        id,
      ej.*
    FROM (
          -- USER DEFINED QUERY MUST RETURN COLUMNS:
          -- pidm, posn, suffix, eff_date
          %s
    ) sub
    JOIN spriden si ON sub.pidm = si.spriden_pidm
          AND si.spriden_change_ind IS NULL
          AND si.spriden_entity_ind = 'P'
    JOIN nbrjobs ej ON sub.pidm = ej.nbrjobs_pidm
          AND sub.posn = ej.nbrjobs_posn
          AND sub.suffix = ej.nbrjobs_suff
          AND sub.eff_date < ej.nbrjobs_effective_date
          AND ej.nbrjobs_status = 'T'
    ''' % query
    cur.execute(sql)
    for row in cur:
        for fld in epFields:
            if (fld.get('apty_code','').startswith(epaf.NORTRAN_JOBEND_PREFIX)
            and fld['field'].lower() in row.keys()):
                fld_name = cur._sanitize(fld['name'], 1)
                ret[row['id']][fld['name']] = row[fld['field'].lower()]

    return sorted(ret.values(), key= lambda x: x['sort_order'])


def main(ref_no, user_id, query_file=None):

    styles = epaf.xl_styles
    epFields = epaf.epafFields(ref_no)
    defValues = {'user_id': user_id}

    if query_file:
        # read query from file
        fp = open('%s/%s' % (epaf.QUERY_DIR.rstrip('/\\'), query_file), 'r')
        query = fp.read()
        empValues = empValsFromQuery(ref_no, query)

    else:
        # Load values from a previous import
        empValues = empValsFromImport(ref_no)
    #xlwt has limit of 256 columns
    cnt = min(len(empValues), 254)

    if cnt == 0:
        print('Dump did not return any rows. Export aborted.')
        sys.exit()

    wb = xlwt.Workbook()
    ws = wb.add_sheet('EPAF Dump')

    ws.write(0,0, 'Fields', styles['head'])
    ws.write(0,1, 'Ref EPAF', styles['head'])
    ws.col(0).width = 256 * 35
    ws.col(1).width = 256 * 25

    # create import column headers and set width
    for c in range(2, cnt + 2):
        ws.write(0, c, 'Import %d' % (c - 1), styles['head'])
        ws.col(c).width = 256 * 25

    r = 1 # row num
    for fld in epFields:
        if fld['type'] == 'head':
            #section header
            ws.write(r, 0, fld['name'], styles['head'])
        else:
            ws.write(r, 0, fld['name'], styles['title'])
            ws.write(r, 1, epaf.valToString(fld['ref_val']), styles['lock'])

            if fld['edit'] == 'N':
                # Can not edit these fields in spreadsheet
                newVal = epaf.valToString(fld['ref_val'])
                newStyle = styles['lock']
            elif fld['name'] in defValues:
                # default a value in
                newVal = epaf.valToString(defValues[fld['name']])
                newStyle = styles[fld['type']]
            else:
                newVal = None
                newStyle = styles[fld['type']]

            # fill in values for import columns
            for i in range(cnt):
                if fld['name'] in empValues[i].keys():
                    ws.write(r, i+2, epaf.valToString(empValues[i][fld['name']]), newStyle)
                elif newVal:
                    ws.write(r, i+2, newVal, newStyle)
                else:
                    ws.write(r, i+2, '', newStyle)

        r += 1

    ws.panes_frozen = True
    ws.vert_split_pos = 2
    ws.horz_split_pos = 1
    ws.protect = True
    wb.save(u'%s/epaf_dump_%d.xls' % (epaf.EXPORT_DIR, ref_no))
    print('Dumped %d records to epaf_dump_%d.xls' % (cnt, ref_no))


if __name__ == '__main__':
    exe_name = os.path.basename(sys.argv[0])
    usage = '''
Usage:\n%s <ONE_UP_NUMBER> <USER_ID>
OR
%s -t\t--transaction <TRANSACTION_NO>
--file <QUERY FILE> (calling w/o query file will dump transactions imported at same time as reference transaction)
--user <USERNAME>
 ''' % (exe_name, exe_name)

    cur = epaf.cur
    parms = {}
    args = []
    opts = []

    try:
        opts, args = getopt.getopt(sys.argv[1:], "ht:",['help','transaction=','file=','user_id='])
    except:
        print(sys.exc_info())
        print(usage)
        sys.exit(1)

    if len(args) == 0 and len(opts) == 0:
        parms = promptForParms()
    elif len(args) == 2:
        # one_up_no, user_id passed as parameters
        parms['one_up'] = int(args[0])
        parms['user_id'] = args[1]

    elif len(args) == 1:
        parms['one_up'] = int(args[0])
    else:
        # process options instead
        for k, v in opts:
            if k in ('-h','--help'):
                print(usage)
                sys.exit()
            elif k in ('-t','--transaction'):
                parms['ref_no'] = int(v)
            elif k in ('--file'):
                parms['file'] = v
            elif k in ('-u','--user_id'):
                parms['user_id'] = v

    if parms.get('one_up', -1) > 0:
        # Need to look up parms from Banner
        sql = '''
        SELECT
          MAX(CASE WHEN pr.gjbprun_number = '01' THEN pr.gjbprun_value END) ref_no,
          MAX(CASE WHEN pr.gjbprun_number = '02' THEN pr.gjbprun_value END) query_file
        FROM gjbprun pr
        WHERE pr.gjbprun_one_up_no = :0
        GROUP BY pr.gjbprun_one_up_no '''
        cur.execute(sql, (parms['one_up'],))
        row = cur.fetchone()
        parms['ref_no'] = int(row['ref_no'])
        parms['file'] = row['query_file']


    if parms.get('ref_no', -1) < 0 :
        print('Error: could not determine the transaction to load')
        sys.exit(1)

    main(parms.get('ref_no'), parms.get('user_id'), parms.get('file'))
