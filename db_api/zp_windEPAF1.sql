CREATE OR REPLACE PACKAGE BODY baninst1.zp_windEPAF AS
  PROCEDURE save_nobtran(
    p_trans_no           NUMBER,
    p_ref_no             NUMBER,
    p_pidm               NUMBER,
    p_effective_date     DATE,
    p_user_id            VARCHAR2
  ) AS
  BEGIN
    MERGE INTO nobtran t
        USING (
          SELECT p_trans_no               nobtran_transaction_no,
            p_pidm                        nobtran_pidm,
            p_effective_date              nobtran_effective_date,
            b.nobtran_acat_code,
            UPPER(p_user_id)              nobtran_user_id
          FROM nobtran b
          WHERE b.nobtran_transaction_no = p_ref_no
        ) s ON (t.nobtran_transaction_no = s.nobtran_transaction_no)
        WHEN MATCHED THEN UPDATE SET
          t.nobtran_effective_date = s.nobtran_effective_date,
          t.nobtran_submittor_user_id = s.nobtran_user_id,
          t.nobtran_originator_user_id  = s.nobtran_user_id,
          t.nobtran_activity_date = SYSDATE
        WHEN NOT MATCHED THEN INSERT (nobtran_transaction_no, nobtran_pidm,
          nobtran_effective_date, nobtran_acat_code, nobtran_trans_status_ind,
          nobtran_submittor_user_id, nobtran_created_date,
          nobtran_originator_user_id, nobtran_submission_date, nobtran_apply_ind,
          nobtran_activity_date)
        VALUES (s.nobtran_transaction_no, s.nobtran_pidm, s.nobtran_effective_date,
          s.nobtran_acat_code, 'P', s.nobtran_user_id, SYSDATE, s.nobtran_user_id,
          SYSDATE, 'N', SYSDATE);
  END save_nobtran;
   
  PROCEDURE save_norrout(
    p_trans_no           NUMBER,
    p_ref_no             NUMBER,
    p_lvl_code           VARCHAR2,
    p_recip_id           VARCHAR2
  ) AS
  BEGIN
    MERGE INTO norrout t 
    USING (
      SELECT p_trans_no norrout_transaction_no,
        COALESCE(p_recip_id, norrout_recipient_user_id) recip_id,
        -- P:pending, I:in queue
        CASE WHEN rn = 1 THEN 'P' ELSE 'I' END norrout_queue_status_ind,
        norrout_alvl_code,
        norrout_action_ind,
        SYSDATE  status_date_time,
        SYSDATE  activity_date, 
        norrout_level_no
      FROM (
        SELECT           
          r.norrout_recipient_user_id,
          r.norrout_alvl_code,
          r.norrout_action_ind,
          r.norrout_level_no,
          row_number() OVER (ORDER BY r.norrout_level_no) rn
        FROM norrout r
        WHERE r.norrout_transaction_no = p_ref_no
      ) sub
      WHERE sub.norrout_alvl_code = p_lvl_code
    ) s ON (t.norrout_transaction_no = s.norrout_transaction_no
      AND t.norrout_alvl_code = s.norrout_alvl_code)
    WHEN MATCHED THEN UPDATE SET
      t.norrout_recipient_user_id = s.recip_id,
      t.norrout_action_user_id = s.recip_id,
      t.norrout_queue_status_ind = s.norrout_queue_status_ind,
      t.norrout_activity_date = SYSDATE
    WHEN NOT MATCHED THEN INSERT (norrout_transaction_no, norrout_recipient_user_id,
      norrout_action_user_id, norrout_alvl_code, norrout_action_ind, norrout_queue_status_ind,
      norrout_status_date_time, norrout_activity_date, norrout_level_no)
    VALUES (s.norrout_transaction_no, s.recip_id, s.recip_id,
      s.norrout_alvl_code, s.norrout_action_ind, s.norrout_queue_status_ind,
      SYSDATE, SYSDATE, s.norrout_level_no);
  END save_norrout;
  
  PROCEDURE save_nortran(
    p_trans_no           NUMBER,
    p_ref_no             NUMBER,
    p_posn               VARCHAR2,
    p_suff               VARCHAR2,
    p_apty_code          VARCHAR2,
    p_field              VARCHAR2,
    p_value              VARCHAR2
  ) AS
  BEGIN
    MERGE INTO nortran t
    USING (
      SELECT p_trans_no         nortran_transaction_no,
        nt.nortran_apty_code,
        CASE WHEN nt.nortran_posn IS NULL THEN NULL ELSE p_posn END nortran_posn,
        CASE WHEN nt.nortran_suff IS NULL THEN NULL ELSE p_suff END nortran_suff,
        nt.nortran_aufm_code,
        nt.nortran_aubk_code,
        nt.nortran_aufd_code,
        p_value   nortran_value,
        nt.nortran_display_seq_no
      FROM nortran nt
      WHERE nt.nortran_transaction_no = p_ref_no
        AND nt.nortran_apty_code = p_apty_code
        AND nt.nortran_aufd_code = p_field
    ) s ON (t.nortran_transaction_no = s.nortran_transaction_no
      AND t.nortran_apty_code = s.nortran_apty_code
      AND t.nortran_aufd_code = s.nortran_aufd_code)
    WHEN MATCHED THEN UPDATE SET
      t.nortran_value = s.nortran_value,
      t.nortran_activity_date = CASE WHEN t.nortran_value <> s.nortran_value THEN SYSDATE ELSE t.nortran_activity_date END
    WHEN NOT MATCHED THEN INSERT (nortran_transaction_no,
      nortran_apty_code, nortran_posn, nortran_suff,
      nortran_aufm_code, nortran_aubk_code,
      nortran_aufd_code, nortran_value, nortran_apply_status_ind,
      nortran_activity_date, nortran_display_seq_no)
    VALUES (
      s.nortran_transaction_no,
      s.nortran_apty_code, s.nortran_posn, s.nortran_suff,
      s.nortran_aufm_code, s.nortran_aubk_code,
      s.nortran_aufd_code, s.nortran_value, 'P',
      SYSDATE, s.nortran_display_seq_no
    );
  END save_nortran;
    
  PROCEDURE save_norcmnt(
    p_trans_no           NUMBER,
    p_user_id            VARCHAR2,
    p_comment            VARCHAR2
  ) AS
  BEGIN
    MERGE INTO norcmnt t
    USING (
        SELECT p_trans_no                                 norcmnt_transaction_no,
          1                                               norcmnt_seq_no,
          p_comment                                       norcmnt_comments,
          p_user_id                                       norcmnt_user_id,
          'HR_EPAF_LOAD'                                  norcmnt_data_origin
        FROM DUAL
    ) s ON (t.norcmnt_transaction_no = s.norcmnt_transaction_no
      AND t.norcmnt_seq_no = s.norcmnt_seq_no)
    WHEN MATCHED THEN
      UPDATE
         SET t.norcmnt_comments       = COALESCE(s.norcmnt_comments, 'DELETE ME'),
             t.norcmnt_user_id        = s.norcmnt_user_id,
             t.norcmnt_activity_date  = CASE WHEN t.norcmnt_comments <> s.norcmnt_comments 
                                        THEN SYSDATE ELSE t.norcmnt_activity_date END
      DELETE WHERE s.norcmnt_comments IS NULL
    WHEN NOT MATCHED THEN
      INSERT
        (norcmnt_transaction_no,
         norcmnt_seq_no,
         norcmnt_comments,
         norcmnt_activity_date,
         norcmnt_user_id,
         norcmnt_data_origin)
      VALUES
        (s.norcmnt_transaction_no,
         s.norcmnt_seq_no,
         s.norcmnt_comments,
         SYSDATE,
         s.norcmnt_user_id,
         s.norcmnt_data_origin)
      WHERE s.norcmnt_comments IS NOT NULL;
  END save_norcmnt;
  
  PROCEDURE save_nortern(
    p_trans_no           NUMBER,
    p_ref_no             NUMBER,
    p_posn               VARCHAR2,
    p_suff               VARCHAR2,
    p_eff_date           DATE,
    p_earn_code          VARCHAR2,
    p_hrs                NUMBER,
    p_special_rate       NUMBER DEFAULT NULL,
		p_cancel_date        DATE,
    p_row_num            NUMBER DEFAULT 1   
  ) AS
  BEGIN
    MERGE INTO nortern t
    USING (
      SELECT sub.*, ee.rid
      FROM (
        SELECT p_trans_no               nortern_transaction_no,
          e.nortern_apty_code,
          p_posn                        nortern_posn,
          p_suff                        nortern_suff,
          p_eff_date                    nortern_effective_date,
          p_earn_code                   nortern_earn_code,
          p_hrs                         nortern_hrs,
          p_special_rate                nortern_special_rate,
          e.nortern_shift               nortern_shift,
          p_cancel_date                 nortern_cancel_date,
          row_number() OVER (ORDER BY e.rowid) rn
        FROM nortern e
        WHERE e.nortern_transaction_no = p_ref_no 
      ) sub
      LEFT JOIN (
         SELECT e.rowid rid, 
           row_number() OVER (ORDER BY e.rowid) rn
         FROM nortern e
         WHERE e.nortern_transaction_no = p_trans_no
      ) ee ON sub.rn = ee.rn
      WHERE sub.rn = p_row_num       
    ) s ON (t.nortern_transaction_no = s.nortern_transaction_no
      AND t.rowid = s.rid)
    WHEN MATCHED THEN UPDATE 
      SET t.nortern_effective_date = s.nortern_effective_date,
          t.nortern_earn_code      = s.nortern_earn_code,
          t.nortern_hrs            = s.nortern_hrs,
          t.nortern_special_rate   = s.nortern_special_rate,
					t.nortern_cancel_date    = s.nortern_cancel_date,
          t.nortern_activity_date  = SYSDATE
    WHEN NOT MATCHED THEN INSERT (nortern_transaction_no, nortern_apty_code,
      nortern_posn, nortern_suff, nortern_effective_date,
      nortern_earn_code, nortern_hrs, nortern_special_rate, nortern_cancel_date,
      nortern_shift, nortern_activity_date, nortern_apply_status_ind)
    VALUES (s.nortern_transaction_no, s.nortern_apty_code,
      s.nortern_posn, s.nortern_suff, s.nortern_effective_date,
      s.nortern_earn_code, s.nortern_hrs, s.nortern_special_rate, 
			s.nortern_cancel_date, s.nortern_shift, SYSDATE, 'P')
    WHERE s.rn = p_row_num;
    END save_nortern;
    
    PROCEDURE save_nortlbd(
      p_trans_no          NUMBER,
      p_ref_no            NUMBER,
      p_posn              VARCHAR2,
      p_suff              VARCHAR2,
      p_eff_date          DATE,
      p_coas_code         VARCHAR2,
      p_fund_code         VARCHAR2,
      p_orgn_code         VARCHAR2,
      p_acct_code         VARCHAR2,
      p_prog_code         VARCHAR2,
      p_actv_code         VARCHAR2,
      p_locn_code         VARCHAR2,
      p_proj_code         VARCHAR2,
      p_pct               NUMBER,
      p_row_num           NUMBER      
    ) AS
    BEGIN
      MERGE INTO nortlbd t
      USING (
        SELECT sub.*, el.rid
        FROM (
          SELECT p_trans_no                nortlbd_transaction_no,
            l.nortlbd_apty_code,
            p_posn                         nortlbd_posn,
            p_suff                         nortlbd_suff,
            p_eff_date                     nortlbd_effective_date,
            COALESCE(p_coas_code, l.nortlbd_coas_code) nortlbd_coas_code,
            COALESCE(p_fund_code, l.nortlbd_fund_code) nortlbd_fund_code,
            COALESCE(p_orgn_code, l.nortlbd_orgn_code) nortlbd_orgn_code,
            COALESCE(p_acct_code, l.nortlbd_acct_code) nortlbd_acct_code,
            COALESCE(p_prog_code, l.nortlbd_prog_code) nortlbd_prog_code,
            COALESCE(p_actv_code, l.nortlbd_prog_code) nortlbd_actv_code,
            COALESCE(p_locn_code, l.nortlbd_prog_code) nortlbd_locn_code,
            COALESCE(p_proj_code, l.nortlbd_prog_code) nortlbd_proj_code,
            COALESCE(p_pct,       l.nortlbd_percent)   nortlbd_percent,
            row_number() OVER (ORDER BY l.rowid) rn
          FROM nortlbd l
          WHERE l.nortlbd_transaction_no = p_ref_no  
        ) sub
        LEFT JOIN (
          SELECT l.rowid rid,
            row_number() OVER (ORDER BY rowid) rn
          FROM nortlbd l
          WHERE l.nortlbd_transaction_no = p_trans_no
        ) el ON sub.rn = el.rn
        WHERE sub.rn = p_row_num
      ) s ON (t.nortlbd_transaction_no = s.nortlbd_transaction_no
        AND t.rowid = s.rid)
      WHEN MATCHED THEN UPDATE
        SET t.nortlbd_posn             = s.nortlbd_posn,
          t.nortlbd_suff               = s.nortlbd_suff,
          t.nortlbd_effective_date     = s.nortlbd_effective_date,
          t.nortlbd_coas_code          = s.nortlbd_coas_code,
          t.nortlbd_fund_code          = s.nortlbd_fund_code,
          t.nortlbd_orgn_code          = s.nortlbd_orgn_code,
          t.nortlbd_acct_code          = s.nortlbd_acct_code,
          t.nortlbd_prog_code          = s.nortlbd_prog_code,
          t.nortlbd_actv_code          = s.nortlbd_actv_code,
          t.nortlbd_locn_code          = s.nortlbd_locn_code,
          t.nortlbd_proj_code          = s.nortlbd_proj_code,
          t.nortlbd_percent            = s.nortlbd_percent,
          t.nortlbd_activity_date      = SYSDATE
      WHEN NOT MATCHED THEN INSERT (nortlbd_transaction_no, nortlbd_apty_code,
        nortlbd_posn, nortlbd_suff, nortlbd_effective_date, nortlbd_coas_code,
        nortlbd_fund_code, nortlbd_orgn_code, nortlbd_acct_code, nortlbd_prog_code,
        nortlbd_actv_code, nortlbd_locn_code, nortlbd_proj_code,
        nortlbd_percent, nortlbd_activity_date, nortlbd_apply_status_ind)
      VALUES (s.nortlbd_transaction_no, s.nortlbd_apty_code,
        s.nortlbd_posn, s.nortlbd_suff, s.nortlbd_effective_date, s.nortlbd_coas_code,
        s.nortlbd_fund_code, s.nortlbd_orgn_code, s.nortlbd_acct_code, s.nortlbd_prog_code,
        s.nortlbd_actv_code, s.nortlbd_locn_code, s.nortlbd_proj_code,
        s.nortlbd_percent, SYSDATE, 'P')
      WHERE s.rn = p_row_num;
  END;
  
  PROCEDURE del_transaction(
    p_trans_no          NUMBER 
  ) AS
    v_status            VARCHAR2(30);
  BEGIN
    SELECT b.nobtran_apply_ind INTO v_status
    FROM nobtran b WHERE b.nobtran_transaction_no = p_trans_no;
    
    IF v_status = 'N' THEN
      DELETE FROM norrout o WHERE o.norrout_transaction_no = p_trans_no;
      DELETE FROM nortran r WHERE r.nortran_transaction_no = p_trans_no;
      DELETE FROM noreaer a WHERE a.noreaer_trans_no       = p_trans_no;
      DELETE FROM norcmnt c WHERE c.norcmnt_transaction_no = p_trans_no;
      DELETE FROM nortlbd l WHERE l.nortlbd_transaction_no = p_trans_no;
      DELETE FROM nortern e WHERE e.nortern_transaction_no = p_trans_no;
      DELETE FROM nobtran b WHERE b.nobtran_transaction_no = p_trans_no;
    END IF;
    EXCEPTION 
      WHEN NO_DATA_FOUND THEN
        -- Transaction was already deleted
        NULL;
  END del_transaction;
END;
/
