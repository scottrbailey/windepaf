CREATE OR REPLACE PACKAGE baninst1.zp_windEPAF AS
  PROCEDURE save_nobtran(
    p_trans_no           NUMBER,
    p_ref_no             NUMBER,
    p_pidm               NUMBER,
    p_effective_date     DATE,
    p_user_id            VARCHAR2
  );
  
  PROCEDURE save_norrout(
    p_trans_no           NUMBER,
    p_ref_no             NUMBER,
    p_lvl_code           VARCHAR2,
    p_recip_id           VARCHAR2
  );
  
  PROCEDURE save_nortran(
    p_trans_no           NUMBER,
    p_ref_no             NUMBER,
    p_posn               VARCHAR2,
    p_suff               VARCHAR2,
    p_apty_code          VARCHAR2,
    p_field              VARCHAR2,
    p_value              VARCHAR2
  );
  
  PROCEDURE save_norcmnt(
    p_trans_no           NUMBER,
    p_user_id            VARCHAR2,
    p_comment            VARCHAR2
  );
  
  PROCEDURE save_nortern(
    p_trans_no           NUMBER,
    p_ref_no             NUMBER,
    p_posn               VARCHAR2,
    p_suff               VARCHAR2,
    p_eff_date           DATE,
    p_earn_code          VARCHAR2,
    p_hrs                NUMBER,
    p_special_rate       NUMBER DEFAULT NULL,
		p_cancel_date        DATE,
    p_row_num            NUMBER DEFAULT 1
  );
  
  PROCEDURE save_nortlbd(
      p_trans_no          NUMBER,
      p_ref_no            NUMBER,
      p_posn              VARCHAR2,
      p_suff              VARCHAR2,
      p_eff_date          DATE,
      p_coas_code         VARCHAR2,
      p_fund_code         VARCHAR2,
      p_orgn_code         VARCHAR2,
      p_acct_code         VARCHAR2,
      p_prog_code         VARCHAR2,
      p_actv_code         VARCHAR2,
      p_locn_code         VARCHAR2,
      p_proj_code         VARCHAR2,
      p_pct               NUMBER,
      p_row_num           NUMBER
  );
  
  PROCEDURE del_transaction(
    p_trans_no          NUMBER 
  );

END;
/
