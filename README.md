# windEPAF
**Bulk EPAF Utilities for Elucian Banner** -
[Download](https://gitlab.com/scottrbailey/windepaf/-/archive/v1.18.7/windepaf-v1.18.7.zip)

Table of Contents:
1. [Summary](#summary)
2. [Process](#process)
3. [Installation](#installation)
4. [Jobsub Installation](#jobsub)
5. [Configuration](#configuration)

<a name="summary">
## Summary
This is a suite of utilities to import Banner EPAFs (Electronic Personnel Action Forms)
from Excel spreadsheets.
The complicated structure of EPAFs make bulk loading them a configuration nightmare. 
Rather than deal with complex configurations, windEPAF gets its configuration
from a reference EPAF and generates a template spreadsheet that has all of
the fields you need to fill out.

<a name="process">
## Process
 1. Create the reference EPAF that is of the correct EPAF category, approval
 routing, and has the maximum number of labor distribution and earnings
 records. Your loaded records can have fewer earnings and labor distribution
 records than the reference, but not more.
 
 2. Generate the Excel template using
    `epaf_export.py --transaction REF_TX_NUMBER --count 100`
    or call
    `EPAF_EXPORT` as a jobsub job. The spreadsheet will be created in the export directory.
 
 3. Fill out the employee information in the generated spreadsheet. Any
 records missing ID, position or effective date will be ignored. When filling
 out spreadsheets there are several short cuts to look up values. Entering __*CV__
 in a field will look up the current value for that employee / posn.
 Entering __*RV__ will look up the value in the reference EPAF. The contract
 type is the one exception, it is always calculated based on whether the employee
 has a primary job active on the effective date. Due to this, you can not
 create two jobs for a new employee in the same spreadsheet as they will both
 be marked primary.
 
 4. Save the spreadsheet to the import directory.

 5. Import the spreadsheet using `epaf_import.py --file FILENAME`
 or run `EPAF_IMPORT` as a jobsub job.

> Tip: On Windows you can navigate to the misc folder in Windows Explorer
> and type "cmd" in the address bar to open a command prompt.
> You can also right click on the epaf_export.py file and choose Open with > Idle.
> From Idle, choose Run > Run Module. You will be prompted for the
> required parameters.

You can also generate pre-populated EPAF spreadsheets from the results of
a query. To do so:
1. Write a SQL query that returns the columns: pidm, posn, suffix, eff_date.
2. Save the SQL file to the windepaf/queries folder.
3. Call `epaf_dump.py -t REF_TX_NUMBER --file=QUERY_FILE_NAME`
or run `EPAF_DUMP` as a jobsub job.

If you find you have made a mistake on a previous import, you can fix it by
extracting, correcting and re-importing them. To do so:
1. Call `epaf_dump.py -t REF_TX_NUMBER` where REF_TX_NUMBER is any of the
transactions you have previously imported.
2. All EPAFs that were imported at the same time as the reference EPAF will
be exported.
3. Make changes and re-import. Previous EPAFs will be updated as long as
the EPAFs have not been cancelled or approved.

<a name="installation">
## Installation
WindEPAF can run either locally on your desktop or as jobs on your
jobsub server. If you already have Python installed and connecting to Oracle,
you can have windEPAF running in minutes.

1. [Download](https://gitlab.com/scottrbailey/windepaf/-/archive/v1.18.7/windepaf-v1.18.7.zip)
 and unzip windEPAF. If installing on the jobsub server, the
files in the misc/ folder will need links created in the _$BANLINKS_ directory.

2. The database package zp_windepaf will need to be compiled on your Banner instance.
As the BANINST1 user, run db_api/install.sql. If running locally, grant execute permission
on zp_windepaf to the appropriate roles or users (see comments in db_api/install.sql
for help.

3. [Python](https://www.python.org/downloads/) will need to be installed
if it is not already. WindEPAF will run on any version of Python from
version 2.6 up. A compatible version of Python is likely already installed
on all Mac and Linux systems. When installing, be sure to check the option
to add Python to the PATH. If you missed that step, Windows will not know
what to do when we try to run a Python file. If you need help adding Python
to your path, see [here.](https://www.pythoncentral.io/add-python-to-path-python-is-not-recognized-as-an-internal-or-external-command/)


4. Install [Oracle Instant Client](http://www.oracle.com/technetwork/database/database-technologies/instant-client/overview/index.html)
if not already installed. If you have the 32-bit version of Python, you need
the 32-bit Instant Client.

5. Several Python packages (cx_Oracle, dbms, xlrd and xlwt) must also be installed.
From the command line you can run:
```
pip install dbms, xlrd, xlwt, cx_Oracle
```

The trickiest part of the setup is getting Python to talk to Oracle.
See the install guide for [cx_Oracle](https://oracle.github.io/python-cx_Oracle/)
for detailed instructions.

<a name="jobsub">
## Jobsub Installation (optional)
These are installed using the same process used to install every other custom jobsub job.
  
1. Follow the directions above to install windEPAF on your jobsub server.
You likely already have Python and Oracle Instant Client installed there.
2. Create _windepaf_ directory with _import_, _export_ and _queries_ subdirectories in your interface directory.
3. Do the normal Job setup routine (GJAJOBS) creating jobs EPAF_EXPORT, EPAF_IMPORT, EPAF_DUMP.
  The system is "H" for Human Resources and the type is "Procedure".
4. Log in as bansecr and set up the security for the jobs.
5. Log back in as your normal user and set up the parameters for each of the jobs in GJAPDEF.
The epaf*.shl files document what jobsub parameters are required.

<a name="configuration">
## Configuration 
All configuration options can be found at the top of the epaf.py file.
The primary configuration is simply telling Python how to connect to the Banner
database. You have three options for doing this:
1. Save a named connection with dbms
    1. If running on the jobsub server, switch to the jobsub user
    2. Open Python and import the dbms package
    3. Set the master password (not the database password) to encrypt and decrypt database passwords
    4. Save a named connection

    ```
    sudo su - jobsub
    python
    ```

    ```python
    >>> import dbms
    >>> # this is not the database password
    >>> dbms.servers.setMasterPassword('CHANGE_ME')
    >>> dbms.servers.save('banner', 'oracle', user='baninst1',
            password='CHANGE_ME_2', database='bandev', host='bandev.mycollege.edu')
    >>> # test the connection
    >>> db = dbms.servers.open('banner')
    >>> cur = db.cursor()
    >>> cur.execute('SELECT * FROM v$version')
    ```
2.  If you already have a script that returns the password for a user, you
can call that from Python. In epaf.py, replace the call to `banner_password.shl`
with the name of your script.
3. Lastly, you can hard code the password in the script (not recommended)

The prefixes for employee start/end and job start/end EPAF blocks can also be
configured. The default is that employee start blocks begin with __ER__,
job start blocks begin with __JR__, employee change/stop blocks begin __ES__ and
job change/stop blocks begin with __JS__. If your EPAF start/end blocks have a discernible
pattern, set it here. If not, dumping pre-populated spreadsheets from a
query may not work.